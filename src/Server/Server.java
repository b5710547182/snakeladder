package Server;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;
import java.awt.Color;
import java.awt.List;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * This is a server for snakeladder game using OCSF framework.
 * It shows player chat log and player log on this server.
 * @author Pipatpol Tanavongchinda
 * @version 2015-05-07 00:28
 */
public class Server extends AbstractServer {
	private List liste;
	private List playersList;
	private Board board;
	private int currentTurn;
	private static final int PORT = 5555;
	private int dice;
	final static int LOGGEDIN = 2;
	final static int PM = 1;
	final static int LOGGEDOUT = 0;
	final static int PLAYING = 5;
	private ArrayList<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	
	public Server(List liste, List playersList) {
		super(PORT);
		this.liste = liste;
		this.dice = 0;
		this.playersList = playersList;
		this.board = new Board();
		this.currentTurn = 1;
	}

	public Server(int port, List liste, List playersList) {
		super(port);
		this.liste = liste;
		this.dice = 0;
		this.board = new Board();
		this.playersList = playersList;
		this.currentTurn = 1;
	}

	@Override
	protected void listeningException(Throwable exception) {
		addLog("Listening exception: " + exception);
		this.liste.setBackground(Color.red);
	}

	@Override
	protected void serverStopped() {
		addLog("Server stopped");
		this.liste.setBackground(Color.yellow);
	}
	
	@Override
	protected void serverClosed() {
		addLog("Server closed");
		this.liste.setBackground(Color.red);
	}

	@Override
	protected void serverStarted() {
		addLog("Server started : " + getPort());
		this.liste.setBackground(Color.green);
	}

	/**
	 * Add log to this server.
	 * @param msg is message that tells what happen on this server.
	 */
	public void addLog(String msg){
		this.liste.add(LocalDateTime.now()+" > "+msg);
		this.liste.makeVisible(this.liste.getItemCount() - 1);
	}

	/**
	 * Show a list of players in server when client login.
	 * @param player is client when login
	 */
	public void showPlayerOnList(String player){
		this.playersList.add(player);
		this.playersList.makeVisible(this.playersList.getItemCount() - 1);
	}
	
	@Override
	protected void clientConnected(ConnectionToClient client) {
		clients.add( client );
		sendToClient(client,"Hello. Please Login");
		client.setInfo("address", client.getInetAddress().getHostAddress());
		addLog(client.getInfo("address")+" has engaged.");
		addLog("Number of clients = "+ this.getNumberOfClients());
		
	}

	@Override
	protected synchronized void clientDisconnected(ConnectionToClient client) {
		addLog(client.getInfo("address")+" has disconnected.");
		clients.remove( client );
		addLog("Number of clients = "+ (this.getNumberOfClients()-1));
		updatePlayersList();
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if(!(msg instanceof String)){
			sendToClient(client,"Unrecognized Message");
		}
		
		if( (Integer)client.getInfo("state") == null ) client.setInfo("state", LOGGEDOUT);
		int state = (Integer)client.getInfo("state");
		String message = (String) msg;
		
		switch (state) {
		case LOGGEDOUT:
			if (message.matches("login \\w+")
					&& !isClientOnline(message.substring(6).trim())) {
				client.setInfo("state", LOGGEDIN);
				client.setInfo("username", message.substring(6).trim());
				sendToClient(client,"System: Welcome, " + client.getInfo("username"));
				sendToAllClients("Notice: " + client.getInfo("username")
						+ " have logged in.");
				showPlayerOnList(client.getInfo("address")+" "+client.getInfo("username"));
			} else if (message.matches("login \\w+") && isClientOnline(message.substring(6).trim())) {
				sendToClient(client,"SysErr: Username already exist, try another.");
			} else {
				sendToClient(client,"System: Please login using \"login [username]\"");
			}
			break;
		case PM:
			int index = (Integer)client.getInfo("whisper");
			if( ((Integer)clients.get(index).getInfo("state") == LOGGEDOUT)){
				sendToClient(client, clients.get(index).getInfo("username")+"is not found.");
			}
			sendToClient(clients.get(index),"["+client.getInfo("username")+"] : "+(String) msg);
			client.setInfo("state", LOGGEDIN);
			break;
		case LOGGEDIN:
			if(message.equalsIgnoreCase("logout")){
				sendToClient(client,"Goodbye");
				super.sendToAllClients(client.getInfo("username")+" has left.");
				addLog((client.getInfo("address")+" logged out as "+client.getInfo("username")));
				clients.remove( client );
				client.setInfo("state", LOGGEDOUT);
			}
			else if (message.length() > 5 && message.substring(0, 6).equalsIgnoreCase("Dice >")){
				sendToClient(client, "Please wait for 4 players and click Start Game");
			}
			else if (message.length() > 2 && message.substring(0, 3).equalsIgnoreCase(("to:"))){
				String username = ((String)msg).substring(4).trim();
				for (ConnectionToClient connectionToClient : clients) {
					if(username.equals((String)connectionToClient.getInfo("username"))){
						client.setInfo("whisper", clients.indexOf(connectionToClient));
						client.setInfo("state", PM);
					}
				}
			}
			else if ( message.equalsIgnoreCase("startgame") && message.length() > 8 && clients.size() == 4) {
				String color = "";
				for (ConnectionToClient connectionToClient : clients) {
					connectionToClient.setInfo("state", PLAYING);
					if(clients.indexOf(connectionToClient) == 0){
						color = "red";
					}
					else if(clients.indexOf(connectionToClient) == 1){
						color = "yellow";
					}
					else if(clients.indexOf(connectionToClient) == 2){
						color = "blue";
					}
					else if(clients.indexOf(connectionToClient) == 3){
						color = "green";
					}
					sendToAllClients(connectionToClient.getInfo("username")+" plays as "+color);
				}
				super.sendToAllClients("Start the game!!");
				sendToAllClients(clients.get(0).getInfo("username")+"\'s turn.");
				addLog("Strat the game!!");
			}
			else if ( message.equalsIgnoreCase("startgame") && message.length() > 8 && clients.size() < 4) {
				sendToClient(client, "Please wait for 4 players and click Start Game");
			}
			else {
				String username = (String) client.getInfo("username");
				addLog(username + " : " + msg);
				super.sendToAllClients(username+" : "+msg);
			}
			break;
		case PLAYING:
			if(board.isGameEnd() == true){
				for (ConnectionToClient connectionToClient : clients) {
					connectionToClient.setInfo("state", LOGGEDIN);
				}
				sendToAllClients("End game : The winner is "+clients.get(board.getWinner()-1).getInfo("username"));
				board.clearAll();
			}
			else if ( message.equalsIgnoreCase("startgame") && message.length() > 8) {
				sendToClient(client, "Game has alreadey started.");
			}
			else if (message.length() > 5 && message.substring(0, 6).equalsIgnoreCase("Dice >")){
				setDice(Integer.parseInt(message.substring(7).trim()));
				this.currentTurn = board.update(clients.indexOf(client),this.dice); 
				String result = String.format("position %s %s %s %s %s %s", 
						board.getPlayer1Pos()+"", 
						board.getPlayer2Pos()+"", 
						board.getPlayer3Pos()+"", 
						board.getPlayer4Pos()+"", 
						String.format("%d", this.dice),
						clients.get(this.currentTurn-1).getInfo("username"));
				sendToAllClients(client.getInfo("username")+" rolled "+this.dice);
				sendToAllClients(result);
			}
			else {
				String username = (String) client.getInfo("username");
				addLog(username + " : " + msg);
				super.sendToAllClients(username+" : "+msg);
			}
			break;
		}
	}
	
	/**
	 * Check this player is online.
	 * @param clientName is name of this clients when they login.
	 * @return true when client is online.
	 */
	private boolean isClientOnline(String clientName) {
		for (ConnectionToClient ctc : clients) {
			if (ctc.getInfo("username") != null
					&& ctc.getInfo("username").toString().equals(clientName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Update the list of players.
	 */
	public void updatePlayersList(){
		this.playersList.removeAll();;
		for (ConnectionToClient connectionToClient : clients) {
			this.playersList.add(connectionToClient.getInfo("address")+" "+connectionToClient.getInfo("username"));
			this.playersList.makeVisible(this.playersList.getItemCount() - 1);
		}
	}

	/**
	 * Set the server dice.
	 * @param value is a value of dice that player roll.
	 */
	public void setDice(int value) {
		this.dice = value;
	}
	
	/**
	 * Get the value of server dice.
	 * @return Value of server dice.
	 */
	public String getDiceValue() {
		return String.format("%d", this.dice);
	}
	
	/**
	 * Send a message to specify client.
	 * @param client is the person that want to send a message.
	 * @param string is message to send.
	 */
	private void sendToClient(ConnectionToClient client, String string) {
		try {
			client.sendToClient(string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
