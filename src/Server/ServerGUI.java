package Server;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

/**
 * This is the serverGUI. 
 * @author Pipatpol Tanavongchinda
 * @version 2015-05-07 00:28
 */
public class ServerGUI extends Frame {
	private Button closeB = new Button("Close");
	private Button listenB = new Button("Listen");
	private Button stopB = new Button("Stop");
	private Button quitB = new Button("Quit");
	private TextField port = new TextField("5555");
	private TextField backlog = new TextField("5");
	private TextField timeout = new TextField("500");
	private Label portLB = new Label("Port: ", 2);
	private Label timeoutLB = new Label("Timeout: ", 2);
	private Label backlogLB = new Label("Backlog: ", 2);
	private List liste = new List();
	private List playersList = new List();
	private Server server;
	private Panel players;
	private Panel bottom;
	private JLabel headerOfPlayers;
	private Panel serverLog;

	public ServerGUI(int p) {
		super("Snakeladder Server");
		this.server = new Server(p, this.liste, this.playersList);
		this.port.setText(String.valueOf(p));
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				ServerGUI.this.quit();
			}
		});
		this.quitB.addActionListener(event -> ServerGUI.this.quit());
		this.closeB.addActionListener(event -> ServerGUI.this.close());
		this.listenB.addActionListener(event -> ServerGUI.this.listen());
		this.stopB.addActionListener(event -> ServerGUI.this.stop());
		addLogPanel();
		addBottomPanel();
		addPlayersList();
		setLayout(new BorderLayout(5, 5));
		add("Center", serverLog);
		add("South", bottom);
		add("West", players);
		setSize(700, 350);
		setVisible(true);
	}
	
	/**
	 * Add log panel to main panel.
	 */
	private void addLogPanel(){
		serverLog = new Panel();
		serverLog.setLayout(new BorderLayout());
		serverLog.add(new JLabel("Log"),BorderLayout.NORTH);
		serverLog.add(this.liste,BorderLayout.CENTER);
	}
	
	/**
	 * Add bottom panel to main panel.
	 */
	private void addBottomPanel(){
		bottom = new Panel();
		bottom.setLayout(new GridLayout(5, 2, 5, 5));
		bottom.add(this.portLB);
		bottom.add(this.port);
		bottom.add(this.backlogLB);
		bottom.add(this.backlog);
		bottom.add(this.timeoutLB);
		bottom.add(this.timeout);
		bottom.add(this.listenB);
		bottom.add(this.stopB);
		bottom.add(this.closeB);
		bottom.add(this.quitB);
	}
	
	/**
	 * Add a player list to main panel.
	 */
	private void addPlayersList(){
		players = new Panel();
		players.setLayout(new BoxLayout(players, BoxLayout.Y_AXIS));
		players.add(headerOfPlayers = new JLabel("Active players"));
		headerOfPlayers.setAlignmentX(CENTER_ALIGNMENT);
		players.add(playersList);
	}

	/**
	 * Read the all the fields.
	 */
	private void readFields() {
		int p = Integer.parseInt(this.port.getText());
		int t = Integer.parseInt(this.timeout.getText());
		int b = Integer.parseInt(this.backlog.getText());

		this.server.setPort(p);
		this.server.setBacklog(b);
		this.server.setTimeout(t);
	}

	/**
	 * Close server.
	 */
	public void close() {
		try {
			readFields();
			this.server.close();
		} catch (Exception ex) {
			this.liste.add(ex.toString());
			this.liste.makeVisible(this.liste.getItemCount() - 1);
			this.liste.setBackground(Color.red);
		}
	}

	/**
	 * Start server and listen from port.
	 */
	public void listen() {
		try {
			readFields();
			this.server.listen();
		} catch (Exception ex) {
			this.liste.add(ex.toString());
			this.liste.makeVisible(this.liste.getItemCount() - 1);
			this.liste.setBackground(Color.red);
		}
	}

	/**
	 * Stop this server
	 */
	public void stop() {
		readFields();
		this.server.stopListening();
	}

	/**
	 * Terminate this UI
	 */
	public void quit() {
		System.exit(0);
	}

	public static void main(String[] arg) {
		ServerGUI localServerFrame;
		if (arg.length == 0) {
			localServerFrame = new ServerGUI(5555);
		}
		if (arg.length == 1) {
			localServerFrame = new ServerGUI(Integer.parseInt(arg[0]));
		}
	}
}