package Server;

import java.io.Serializable;

/**
 * This is a board for snakeladder game.
 * It controls players' position on this server.
 * @author Thanawit Gerdpraserd
 * @version 2015-05-07 01:11
 */
public class Board implements Serializable{
	private int player1Pos;
	private int player2Pos;
	private int player3Pos;
	private int player4Pos;
	private boolean player2turn;
	private boolean player1turn;
	private boolean player3turn;
	private boolean player4turn;
	private boolean gameEnd;
	private int winner;
	
	public Board() {
		setPlayer1Pos(0);
		setPlayer2Pos(0);
		setPlayer3Pos(0);
		setPlayer4Pos(0);
		this.player1turn = true;
		this.player2turn = false;
		this.player3turn = false;
		this.player4turn = false;
		this.gameEnd = false;
	}
	
	/**
	 * Get player 1 position.
	 * @return this player position.
	 */
	public int getPlayer1Pos() {
		return player1Pos;
	}

	/*
	 * Set player 1 position.
	 */
	public void setPlayer1Pos(int player1Pos) {
		this.player1Pos = player1Pos;
	}

	/**
	 * Get player 2 position.
	 * @return this player position.
	 */
	public int getPlayer2Pos() {
		return player2Pos;
	}

	/*
	 * Set player 2 position.
	 */
	public void setPlayer2Pos(int player2Pos) {
		this.player2Pos = player2Pos;
	}
	
	/**
	 * Get player 3 position.
	 * @return this player position.
	 */
	public int getPlayer3Pos() {
		return player3Pos;
	}

	/*
	 * Set player 3 position.
	 */
	public void setPlayer3Pos(int player3Pos) {
		this.player3Pos = player3Pos;
	}

	/**
	 * Get player 4 position.
	 * @return this player position.
	 */
	public int getPlayer4Pos() {
		return player4Pos;
	}

	/*
	 * Set player 4 position.
	 */
	public void setPlayer4Pos(int player4Pos) {
		this.player4Pos = player4Pos;
	}

	/**
	 * Update position of all player on board.
	 * @param playerNum is order of player.
	 * @param diceValue is value of dice.
	 * @return Turn of player.
	 */
	public int update(int playerNum, int diceValue){
		if(getPlayer1Pos() == 64 || getPlayer2Pos() == 64 || getPlayer3Pos() == 64 || getPlayer4Pos() ==64){
			this.gameEnd = true;
			if(getPlayer1Pos() == 64){
				this.winner = 1;
			}
			else if(getPlayer2Pos() == 64){
				this.winner = 2;
			}
			else if(getPlayer3Pos() == 64){
				this.winner = 3;
			}
			else if(getPlayer4Pos() == 64){
				this.winner = 4;
			}
		}
		if(playerNum == 0 && this.player1turn == true){
			int thisPos = trapCheck(getPlayer1Pos()+diceValue);
			setPlayer1Pos(thisPos);
			this.player1turn = false;
			this.player2turn = true;
			return 2;
		}
		else if(playerNum == 1 && this.player2turn == true){
			int thisPos = trapCheck(getPlayer2Pos()+diceValue);
			setPlayer2Pos(thisPos);
			this.player2turn = false;
			this.player3turn = true;
			return 3;
		}
		else if(playerNum == 2 && this.player3turn == true){
			int thisPos = trapCheck(getPlayer3Pos()+diceValue);
			setPlayer3Pos(thisPos);
			this.player3turn = false;
			this.player4turn = true;
			return 4;
		}
		else if(playerNum == 3 && this.player4turn == true){
			int thisPos = trapCheck(getPlayer4Pos()+diceValue);
			setPlayer4Pos(thisPos);
			this.player4turn = false;
			this.player1turn = true;
			return 1;
		}
		return 0;
	}
	
	/**
	 * Reset game.
	 */
	public void clearAll(){
		this.player1turn = true;
		this.player2turn = false;
		this.player3turn = false;
		this.player4turn = false;
		this.gameEnd = false;
		setPlayer1Pos(0);
		setPlayer2Pos(0);
		setPlayer3Pos(0);
		setPlayer4Pos(0);
	}
	
	/**
	 * Get the winner.
	 * @return The winner of this round.
	 */
	public int getWinner() {
		return this.winner;
	}
	
	/**
	 * Check game is end.
	 * @return true if game end.
	 */
	public boolean isGameEnd(){
		return this.gameEnd;
	}
	
	/**
	 * Check the trap.
	 * @param position is the position of player.
	 * @return new position of player.
	 */
	public int trapCheck(int position){
		if(position == 1)
			return 16;
		else if(position == 2)
			return 14;
		else if(position == 5)
			return 22;
		else if(position == 13)
			return 4;
		else if(position == 15)
			return 3;
		else if(position == 31)
			return 17;
		else if(position == 33)
			return 63;
		else if(position == 37)
			return 21;
		else if(position == 38)
			return 28;
		else if(position == 50)
			return 34;
		else if(position == 51)
			return 35;
		else if(position == 52)
			return 36;
		else if(position == 59)
			return 42;
		else if(position == 61)
			return 29;
		else if(position == 63)
			return 33;
		else if(position  > 64 ) {
			int currentPos = 0;
			int remain = position - 64; 
			currentPos = 64 - remain;
			return currentPos;
		}
		else
			return position;
	}

}
