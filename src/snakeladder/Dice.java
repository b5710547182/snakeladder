package snakeladder;

import static java.lang.Math.*;

/**
 * This is a dice.
 * @author Pipatpol Tanavongchinda
 * @version 2015-05-07 01:10
 */
public class Dice {
	private int value; 
	
	public Dice() {
		this.value = 0;
	}
	
	/**
	 * Roll the dice
	 */
	public void roll(){
		value = (int) (floor(random()*6)+1);
	}
	
	/**
	 * Get the dice value.
	 */
	public String toString() {
		return String.format("%d", this.value);
	}
}
