package snakeladder;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JFrame;

/**
 * This is the ClientUI.
 * @author Pipatpol Tanavongchinda
 * @version 2015-05-07 00:50 
 */
public class ClientUI extends JFrame {
	private static final int DEFAULT_PORT = 5555;
	private static final String DEFAULT_HOST = "localhost";
	private Button closeButton;
	private Button openButton;
	private Button sendButton;
	private Button startGameButton;
	public Button rollButton;
	private TextField portField;
	private TextField hostField;
	private TextField messageField;
	private Label portLabel;
	private Label hostLabel;
	private Font font = new Font("Angsana New", 0, 16);
	private List liste;
	private Client client;
	private int port = 5555;
	private String host = "localhost";
	private Panel hostPanel;
	private Font messageFont;
	private Panel buttonPanel;
	private Panel southPanel;
	private Panel msgPanel;
	private Label showDice;

	public ClientUI(String host, int port) {
		super("Snakeladder Client");
		if ((host != null) && (host.length() > 0)) {
			this.host = host;
		}
		if (port > 0) {
			this.port = port;
		}
		this.liste = new List();

		initComponents();

		Dice dice = new Dice();

		this.client = new Client(host, port, this.liste, this);
		this.portField.setText(String.valueOf(port));
		this.hostField.setText(host);

		this.startGameButton.addActionListener(click -> {
			try {
				client.sendToServer("startgame");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		this.closeButton.addActionListener(click -> ClientUI.this.close());
		this.openButton.addActionListener(click -> ClientUI.this.open());
		this.rollButton.addActionListener(click -> {
			dice.roll();
			try {
				client.sendToServer("Dice > " + dice.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		ActionListener messageActionListener = click -> {
			ClientUI.this.send();
		};
		this.sendButton.addActionListener(messageActionListener);
		this.messageField.addActionListener(messageActionListener);
	}

	/**
	 * All component of this UI.
	 */
	private void initComponents() {
		int gap = 5;
		addButtonAndTextField();
		addHostPanel();
		addMsgPanel();
		addButtonPanel();
		addSouthPanel();
		Component[] arrayOfComponent;
		int j = (arrayOfComponent = hostPanel.getComponents()).length;
		for (int i = 0; i < j; i++) {
			Component c = arrayOfComponent[i];
			c.setFont(this.font);
		}
		j = (arrayOfComponent = buttonPanel.getComponents()).length;
		for (int i = 0; i < j; i++) {
			Component c = arrayOfComponent[i];
			c.setFont(this.font);
		}
		this.liste.setFont(messageFont);
		setLayout(new BorderLayout(5, 5));
		add("North", hostPanel);
		add("Center", this.liste);
		add("South", southPanel);
		pack();
		setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Add south panel to this ClientUI.
	 */
	private void addSouthPanel() {
		southPanel = new Panel();
		southPanel.setLayout(new GridLayout(2, 1, 5, 5));
		southPanel.add(msgPanel);
		southPanel.add(buttonPanel);
	}

	/**
	 * Add button panel to this ClientUI.
	 */
	private void addButtonPanel() {
		buttonPanel = new Panel();
		buttonPanel.setLayout(new GridLayout(1, 4, 5, 5));
		buttonPanel.add(this.sendButton);
		buttonPanel.add(this.startGameButton);
		buttonPanel.add(this.rollButton);
		buttonPanel.add(this.showDice);
	}

	/**
	 * Add message panel to this ClientUI.
	 */
	private void addMsgPanel() {
		msgPanel = new Panel();
		msgPanel.setLayout(new GridLayout(1, 1));
		msgPanel.add(this.messageField);
		this.messageField.setFont(messageFont);
	}

	/**
	 * Add host panel to this ClientUI.
	 */
	private void addHostPanel() {
		hostPanel = new Panel();
		hostPanel.setLayout(new FlowLayout(0, 5, 5));
		hostPanel.add(this.hostLabel);
		hostPanel.add(this.hostField);
		hostPanel.add(this.portLabel);
		hostPanel.add(this.portField);
		hostPanel.add(this.openButton);
		hostPanel.add(this.closeButton);
	}

	/**
	 * Add button and textfield to this CLientUI.
	 */
	private void addButtonAndTextField() {
		messageFont = new Font("DialogInput", 0, 18);
		this.closeButton = new Button("Close");
		this.openButton = new Button("Open");
		this.sendButton = new Button("Send");
		this.startGameButton = new Button("Start Game");
		this.portField = new TextField(8);
		this.portField.setText(Integer.toString(this.port));
		this.hostField = new TextField(24);
		this.hostField.setText(this.host);
		this.messageField = new TextField();
		this.portLabel = new Label("Port:", 2);
		this.hostLabel = new Label("Host:", 2);
		this.rollButton = new Button("Roll");
		this.showDice = new Label("0");
	}

	/**
	 * Read port and IP.
	 */
	private void readFields() {
		int p = Integer.parseInt(this.portField.getText());
		this.client.setPort(p);
		this.client.setHost(this.hostField.getText());
	}

	/**
	 * Show the messages from server.
	 * @param word is messages.
	 */
	public void showMessage(String word) {
		this.liste.add(word);
		this.liste.makeVisible(this.liste.getItemCount() - 1);
	}

	/**
	 * Close connection with server.
	 */
	public void close() {
		try {
			readFields();
			this.client.closeConnection();
		} catch (Exception ex) {
			showMessage(ex.toString());
			this.liste.setBackground(Color.red);
		}
	}

	/**
	 * Set the dice of this client.
	 * @param value
	 */
	public void setDice(String value) {
		this.showDice.setText(value);
	}

	/**
	 * Open connection to server.
	 */
	public void open() {
		try {
			readFields();
			this.client.openConnection();
		} catch (Exception ex) {
			showMessage(ex.toString());
			this.liste.setBackground(Color.red);
		}
	}

	/**
	 * Send a message to server.
	 */
	public void send() {
		try {
			this.client.sendToServer(this.messageField.getText());
			this.messageField.setText("");
		} catch (Exception ex) {
			showMessage(ex.toString());
			this.liste.setBackground(Color.yellow);
		}
	}

	public static void main(String[] arg) {
		ClientUI localClientFrame;
		if (arg.length == 0) {
			localClientFrame = new ClientUI("localhost", 5555);
		}
		if (arg.length == 1) {
			localClientFrame = new ClientUI("localhost",
					Integer.parseInt(arg[0]));
		}
		if (arg.length == 2) {
			localClientFrame = new ClientUI(arg[0], Integer.parseInt(arg[1]));
		}
	}
}