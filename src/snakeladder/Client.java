package snakeladder;

import Server.Board;
import com.lloseng.ocsf.client.AbstractClient;
import java.awt.Color;
import java.awt.List;

/**
 * This is a client for snakeladder game using OCSF framework.
 * @author Pipatpol Tanavongchinda
 * @version 2015-05-07 01:01
 */
public class Client extends AbstractClient {
	private List liste;
	private ClientUI clientFrame;
	public BoardUI boardUI;
	private int currentPosition;
	private Board board;
	private String whoseTurn;

	public Client(List liste, ClientUI frame) {
		super("localhost", 12345);
		this.liste = liste;
		this.clientFrame = frame;
		this.currentPosition = 0;
		this.boardUI = BoardUI.getInstance();
		this.boardUI.setVisible(false);
		this.board = new Board();
	}

	public Client(int port, List liste, ClientUI frame) {
		super("localhost", port);
		this.liste = liste;
		this.clientFrame = frame;
		this.currentPosition = 0;
		this.boardUI = BoardUI.getInstance();
		this.boardUI.setVisible(false);
		this.board = new Board();
	}

	public Client(String host, int port, List liste, ClientUI frame) {
		super(host, port);
		this.liste = liste;
		this.clientFrame = frame;
		this.currentPosition = 0;
		this.boardUI = BoardUI.getInstance();
		this.boardUI.setVisible(false);
		this.board = new Board();
	}

	/**
	 * Show all message from server.
	 * @param word is the message.
	 */
	public void showMessage(String word) {
		this.liste.add(word);
		this.liste.makeVisible(this.liste.getItemCount() - 1);
	}

	@Override
	protected void connectionClosed() {
		showMessage("**Connection closed**");
		this.liste.setBackground(Color.pink);
	}

	@Override
	protected void connectionException(Exception exception) {
		showMessage("**Connection exception: " + exception);
		this.liste.setBackground(Color.red);
	}

	@Override
	protected void connectionEstablished() {
		showMessage("--Connection established");
		this.liste.setBackground(Color.green);
		this.boardUI.setVisible(true);
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		String message = (String) msg;
		if( message.length() > 7 && message.substring(0, 8).equalsIgnoreCase(("position"))){
			String[] pos = message.split(" ");
			this.board.setPlayer1Pos(Integer.parseInt(pos[1]));
			this.board.setPlayer2Pos(Integer.parseInt(pos[2]));
			this.board.setPlayer3Pos(Integer.parseInt(pos[3]));
			this.board.setPlayer4Pos(Integer.parseInt(pos[4]));
			this.clientFrame.setDice(pos[5]);
			this.whoseTurn = pos[6];
			showMessage(whoseTurn+"\'s turn.");
			this.boardUI.handleUpdate(this.board);
		}

		else{
			showMessage(msg.toString());
			if (message.length() > 9 &&message.substring(message.length()-8, message.length()-2).equalsIgnoreCase("Dice >")){
				this.clientFrame.setDice(message.substring(message.length()-1).trim());
			}
		}
	}

}