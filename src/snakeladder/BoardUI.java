package snakeladder;

import java.net.URL;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import Server.Board;

/**
 * This is boardUI use for show the players on board.
 * @author Thanawit Gerdpraserd
 * @version 2015-05-07 01.03
 */
public class BoardUI extends JFrame {

	private static BoardUI boardUI;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BoardUI frame = BoardUI.getInstance();
		frame.setVisible(true);
		int player =0 ;
		Scanner scanner = new Scanner(System.in);
		System.out.println(",");
		do{
			System.out.print("Enter Player : ");
			player = scanner.nextInt();
			System.out.print("Enter Position : ");
			int position = scanner.nextInt();
			frame.update(player,position);
		}while
			(player!=0);

		scanner.close();

	}

	/**
	 * Create the frame.
	 */

	private JLabel Player1;
	private JLabel Player2;
	private JLabel Player3;
	private JLabel Player4;

	private BoardUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponent();
	}

	/**
	 * All component of this UI.
	 */
	public void initComponent(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 618);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		ClassLoader loader = this.getClass().getClassLoader();
		URL player1_URL = loader.getResource("project images/Player1.png");
		ImageIcon player1_img = new ImageIcon( player1_URL );
		Player1 = new JLabel("Player1");
		Player1.setIcon(player1_img);
		Player1.setBounds(30, 520, 30, 30);
		contentPane.add(Player1);

		URL player2_URL = loader.getResource("project images/Player2.png");
		ImageIcon player2_img = new ImageIcon( player2_URL );
		Player2 = new JLabel("Player2");
		Player2.setIcon(player2_img);
		Player2.setBounds(70, 520, 30, 30);
		contentPane.add(Player2);

		URL player3_URL = loader.getResource("project images/Player3.png");
		ImageIcon player3_img = new ImageIcon( player3_URL );
		Player3 = new JLabel("Player3");
		Player3.setIcon(player3_img);
		Player3.setBounds(30, 550, 30, 30);
		contentPane.add(Player3);

		URL player4_URL = loader.getResource("project images/Player4.png");
		ImageIcon player4_img = new ImageIcon( player4_URL );
		Player4 = new JLabel("Player4");
		Player4.setIcon(player4_img);
		Player4.setBounds(70, 550, 30, 30);
		contentPane.add(Player4);

		URL board_URL = loader.getResource("project images/BoardGame.png");
		ImageIcon board_img = new ImageIcon( board_URL );
		JLabel Board = new JLabel("Board");
		Board.setIcon(board_img);
		Board.setBounds(0, 0, 800, 600);
		contentPane.add(Board);
	}
	
	public void handleUpdate(Board board){
		update(1,board.getPlayer1Pos());
		update(2,board.getPlayer2Pos());
		update(3,board.getPlayer3Pos());
		update(4,board.getPlayer4Pos());
	}
	
	/**
	 * Update position of player on the board.
	 * @param playerNumber is order of player.
	 * @param position is position of this player.
	 */
	public void update(int playerNumber, int position){

		int checkedPosition = position;
		int posX =adjustPosX( checkedPosition ) ;
		int posY =adjustPosY ( checkedPosition ) ;
		int X = handlePosX( playerNumber , posX );
		int Y = handlePosY( playerNumber , posY );

		if(playerNumber == 1){
			Player1.setBounds(X, Y, 30, 30);
		}
		else if(playerNumber ==2){
			Player2.setBounds(X, Y, 30, 30);
		}
		else if(playerNumber == 3 ){
			Player3.setBounds(X, Y, 30, 30);
		}
		else if(playerNumber ==4 ){
			Player4.setBounds(X, Y, 30, 30);
		}
		else {
			//do nothing  
		}

		if(position == 64){
			String color = "";
			if(playerNumber-1 == 0){
				color = "red";
			}
			else if(playerNumber-1 == 1){
				color = "yellow";
			}
			else if(playerNumber-1 == 2){
				color = "blue";
			}
			else if(playerNumber-1 == 3){
				color = "green";
			}
			JOptionPane.showMessageDialog(this , "Player " +  color + " WIN !!!" );
		}
	}

	/**
	 * Check the trap of this board.
	 * @param position is position of player.
	 * @return new position.
	 */
	public int trapCheck(int position){
		if(position == 1)
			return 16;
		else if(position == 2)
			return 14;
		else if(position == 5)
			return 22;
		else if(position == 13)
			return 4;
		else if(position == 15)
			return 3;
		else if(position == 31)
			return 17;
		else if(position == 33)
			return 63;
		else if(position == 37)
			return 21;
		else if(position == 38)
			return 28;
		else if(position == 50)
			return 34;
		else if(position == 51)
			return 35;
		else if(position == 52)
			return 36;
		else if(position == 59)
			return 42;
		else if(position == 61)
			return 29;
		else if(position == 63)
			return 33;
		else
			return position;
	}

	/**
	 * Adjust position X.
	 * @param position is the current position of player.
	 * @return position of this player on board.
	 */
	public int adjustPosX(int position){
		int pos1 = 1;
		int pos2 = 16;
		int pos3 = 17;
		int pos4 = 32;
		int pos5 = 33;
		int pos6 = 48;
		int pos7 = 49;
		int pos8 = 64;
		if(position ==pos1 ||position ==pos2||position ==pos3||position ==pos4||position ==pos5||position ==pos6||position ==pos7||position ==pos8){
			return 1;
		}
		else if(position == (pos1+1) ||position ==(pos2-1)||position ==(pos3+1)||position ==(pos4-1)||position ==(pos5+1)||position ==(pos6-1)||position ==(pos7+1)||position ==(pos8-1)){
			return 2;
		}
		else if(position == (pos1+2) ||position ==(pos2-2)||position ==(pos3+2)||position ==(pos4-2)||position ==(pos5+2)||position ==(pos6-2)||position ==(pos7+2)||position ==(pos8-2)){
			return 3;
		}
		else if(position == (pos1+3) ||position ==(pos2-3)||position ==(pos3+3)||position ==(pos4-3)||position ==(pos5+3)||position ==(pos6-3)||position ==(pos7+3)||position ==(pos8-3)){
			return 4;
		}
		else if(position == (pos1+4) ||position ==(pos2-4)||position ==(pos3+4)||position ==(pos4-4)||position ==(pos5+4)||position ==(pos6-4)||position ==(pos7+4)||position ==(pos8-4)){
			return 5;
		}
		else if(position == (pos1+5) ||position ==(pos2-5)||position ==(pos3+5)||position ==(pos4-5)||position ==(pos5+5)||position ==(pos6-5)||position ==(pos7+5)||position ==(pos8-5)){
			return 6;
		}
		else if(position == (pos1+6) ||position ==(pos2-6)||position ==(pos3+6)||position ==(pos4-6)||position ==(pos5+6)||position ==(pos6-6)||position ==(pos7+6)||position ==(pos8-6)){
			return 7;
		}
		else if(position == (pos1+7) ||position ==(pos2-7)||position ==(pos3+7)||position ==(pos4-7)||position ==(pos5+7)||position ==(pos6-7)||position ==(pos7+7)||position ==(pos8-7)){
			return 8;
		}
		else
			return -1;
	}

	/**
	 * Adjust position Y.
	 * @param position is the current position of player.
	 * @return position of this player on board.
	 */
	public int adjustPosY(int position){

		int pos1 = 1; 
		int pos2 = 9; 
		int pos3 = 17;
		int pos4 = 25;
		int pos5 = 33;
		int pos6 = 41;
		int pos7 = 49;
		int pos8 = 57;
		if(position ==pos1 ||position == (pos1+1 )||position ==(pos1+2)||position ==(pos1+3)||position ==(pos1+4)||position ==(pos1+5)||position ==(pos1+6)||position ==(pos1+7)){
			return 1;
		}
		else if(position ==pos2 ||position == (pos2+1 )||position ==(pos2+2)||position ==(pos2+3)||position ==(pos2+4)||position ==(pos2+5)||position ==(pos2+6)||position ==(pos2+7)){
			return 2;
		}
		else if(position ==pos3 ||position == (pos3+1 )||position ==(pos3+2)||position ==(pos3+3)||position ==(pos3+4)||position ==(pos3+5)||position ==(pos3+6)||position ==(pos3+7)){
			return 3;
		}
		else if(position ==pos4 ||position == (pos4+1 )||position ==(pos4+2)||position ==(pos4+3)||position ==(pos4+4)||position ==(pos4+5)||position ==(pos4+6)||position ==(pos4+7)){
			return 4;
		}
		else if(position ==pos5 ||position == (pos5+1 )||position ==(pos5+2)||position ==(pos5+3)||position ==(pos5+4)||position ==(pos5+5)||position ==(pos5+6)||position ==(pos5+7)){
			return 5;
		}
		else if(position ==pos6 ||position == (pos6+1 )||position ==(pos6+2)||position ==(pos6+3)||position ==(pos6+4)||position ==(pos6+5)||position ==(pos6+6)||position ==(pos6+7)){
			return 6;
		}
		else if(position ==pos7 ||position == (pos7+1 )||position ==(pos7+2)||position ==(pos7+3)||position ==(pos7+4)||position ==(pos7+5)||position ==(pos7+6)||position ==(pos7+7)){
			return 7;
		}
		else if(position ==pos8 ||position == (pos8+1 )||position ==(pos8+2)||position ==(pos8+3)||position ==(pos8+4)||position ==(pos8+5)||position ==(pos8+6)||position ==(pos8+7)){
			return 8;
		}
		else
			return -1;

	}


	public int handlePosX(int playerNumber , int position ){
		int X = 0;
		position-=1;
		if(playerNumber == 1 || playerNumber == 3){
			X = 30;
			position*=100;
			X+=position;
		}
		else if(playerNumber ==2 || playerNumber == 4){
			X = 70;
			position*=100;
			X+=position;
		}
		else {
			//do nothing  
		}

		return X;
	}

	/**
	 * Get instance of this class.
	 * @return
	 */
	public static BoardUI getInstance(){
		if(boardUI==null)
		{
			boardUI = new BoardUI();
		}
		return boardUI;
	}


	/**
	 * return the Y position.
	 * @param playerNumber is order of this player.
	 * @param position is current position of this player.
	 * @return y position.
	 */
	public int handlePosY(int playerNumber , int position ){
		int Y =0;
		position-=1;
		if(playerNumber == 1 || playerNumber ==2 ){
			Y = 520;
			position*=70;
			position*=-1;
			Y+=position;
		}
		else if(playerNumber ==3 || playerNumber == 4){
			Y = 550;
			position*=70;
			position*=-1;
			Y+=position;
		}
		return Y;
	}
}