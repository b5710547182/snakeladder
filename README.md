**Snake Ladder Game**
===========================

Project Proposal by 
---------------------------

[Thanawit Gerdprasert	5710547182](https://bitbucket.org/b5710547182)

[Pipatpol Tanavongchinda 5710546372](https://bitbucket.org/b5710546372)

Example image of the game: 

![ladderssnakesboardgamescreenshot.jpg](https://bitbucket.org/repo/ryLL6X/images/407713758-ladderssnakesboardgamescreenshot.jpg) 

#Description
--------------------------
In our OOP2 project, we decide that we will do the Online Multiplayer Snake Ladder Game.

#Feature
--------------------------
We plan to use OCSF in order to make our game be able to play from multiplayer.    
Most of our game probably consist of a lot of basic GUI component.
We set our goal to have at least two-player multiplayer but if we able to finish in time we might extends to the 4 player game and make a list of rooms that people more than 4 can access to play in the same time with another room.

#System Requires
-------------------------
Java SE Runtime Environment 8 or Up.

Internet.

Free space 3 MB.